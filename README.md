## Raw data source
`http http://www.unitbv.ro/Studenti/Campusuniversitar/Camine/Cazarecamine.aspx > data/cazarecamine.html`

## Faculties
### Faculty names  
`xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/strong/span/text()' --data=data/cazarecamine.html`

```
INGINERIE MECANICA
MATEMATICA SI INFORMATICA
INGINERIE TEHNOLOGICA SI MANAGEMENT INDUSTRIAL
STIINTE ECONOMICE SI ADMINISTRAREA AFACERILOR
DESIGN DE PRODUS SI MEDIU
LITERE
STIINTA SI INGINERIA MATERIALELOR
DREPT
INGINERIE ELECTRICA SI STIINTA CALCULATOARELOR
SOCIOLOGIE SI COMUNICARE
SILVICULTURA SI EXPLOATARI FORESTIERE
MEDICINA
INGINERIA LEMNULUI
PSIHOLOGIE SI STIINTELE EDUCATIEI
CONSTRUCTII
EDUCATIE FIZICA SI SPORTURI MONTANE
ALIMENTATIE SI TURISM
MUZICA
```

### Faculty link logos
`xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/(span|(strong/span))/img/@src' --data=data/cazarecamine.html`

```
http://www.unitbv.ro/portals/0/Images/sigle/IM.png
/portals/0/Images/sigle/logo-MIen%20nou.jpg
/portals/0/Images/sigle/sigla_ITMI.png
http://www.unitbv.ro/portals/0/Images/sigle/seaa.jpg
http://www.unitbv.ro/portals/0/Images/sigle/dpm.jpg
http://www.unitbv.ro/portals/0/Images/sigle/litere.jpg
/portals/0/Images/sigle/sigla%20sim.jpg
/portals/0/Images/sigle/sigla%20DR.JPG
http://www.unitbv.ro/portals/0/Images/sigle/IESC.png
http://www.unitbv.ro/portals/0/Images/sigle/socio.png
http://www.unitbv.ro/portals/0/Images/sigle/silvicultura.png
/portals/0/Images/sigle/medicina%20color.png
http://www.unitbv.ro/portals/0/Images/sigle/ilemn.png
http://www.unitbv.ro/portals/0/Images/sigle/psiho.jpg
http://www.unitbv.ro/portals/0/Images/sigle/constructii.png
http://www.unitbv.ro/portals/0/Images/sigle/sport.png
http://www.unitbv.ro/portals/0/Images/sigle/alimturism.jpg
http://www.unitbv.ro/portals/0/Images/sigle/muzica.png
```

### Faculty name - link logos
`paste -d@ <(xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/strong/span/text()' --data=cazarecamine.html) <(xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/(span|(strong/span))/img/@src' --data=data/cazarecamine.html)`

```
INGINERIE MECANICA@http://www.unitbv.ro/portals/0/Images/sigle/IM.png
MATEMATICA SI INFORMATICA@/portals/0/Images/sigle/logo-MIen%20nou.jpg
INGINERIE TEHNOLOGICA SI MANAGEMENT INDUSTRIAL@/portals/0/Images/sigle/sigla_ITMI.png
STIINTE ECONOMICE SI ADMINISTRAREA AFACERILOR@http://www.unitbv.ro/portals/0/Images/sigle/seaa.jpg
DESIGN DE PRODUS SI MEDIU@http://www.unitbv.ro/portals/0/Images/sigle/dpm.jpg
LITERE@http://www.unitbv.ro/portals/0/Images/sigle/litere.jpg
STIINTA SI INGINERIA MATERIALELOR@/portals/0/Images/sigle/sigla%20sim.jpg
DREPT@/portals/0/Images/sigle/sigla%20DR.JPG
INGINERIE ELECTRICA SI STIINTA CALCULATOARELOR@http://www.unitbv.ro/portals/0/Images/sigle/IESC.png
SOCIOLOGIE SI COMUNICARE@http://www.unitbv.ro/portals/0/Images/sigle/socio.png
SILVICULTURA SI EXPLOATARI FORESTIERE@http://www.unitbv.ro/portals/0/Images/sigle/silvicultura.png
MEDICINA@/portals/0/Images/sigle/medicina%20color.png
INGINERIA LEMNULUI@http://www.unitbv.ro/portals/0/Images/sigle/ilemn.png
PSIHOLOGIE SI STIINTELE EDUCATIEI@http://www.unitbv.ro/portals/0/Images/sigle/psiho.jpg
CONSTRUCTII@http://www.unitbv.ro/portals/0/Images/sigle/constructii.png
EDUCATIE FIZICA SI SPORTURI MONTANE@http://www.unitbv.ro/portals/0/Images/sigle/sport.png
ALIMENTATIE SI TURISM@http://www.unitbv.ro/portals/0/Images/sigle/alimturism.jpg
MUZICA@http://www.unitbv.ro/portals/0/Images/sigle/muzica.png
```

### Import faculties into PostgreSQL
`paste -d@ <(xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/strong/span/text()' --data=data/cazarecamine.html) <(xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/(span|(strong/span))/img/@src' --data=data/cazarecamine.html) | awk -F'@' 'BEGIN { OFS="@"; } { if ($2 ~ /^\//) { $2="http://www.unitbv.ro"$2 } print $1, $2 }' | psql -h localhost -d studnet -U postgres -c "\COPY faculties(name, logo) FROM STDIN DELIMITER AS '@'"`

## Accomodations
### Accommodation PDFs
`xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/strong/span/text() | //*[@id="Facultati"]/tbody/tr/td/p/strong/a/@href' --data=data/cazarecamine.html | awk '!a[$0]++'`

```
INGINERIE MECANICA
/Portals/0/Studenti/Cazari 2014-2015/CAZARE FAZA I   IM 2014.pdf
MATEMATICA SI INFORMATICA
/Portals/0/Studenti/Cazari 2014-2015/Cazare MI 2014 faza I.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazare MI faza II.pdf
INGINERIE TEHNOLOGICA SI MANAGEMENT INDUSTRIAL
/Portals/0/Studenti/Cazari 2014-2015/Cazari Facultatea ITMI 2014 - 2015 (Faza I - pe camere).pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari ITMI faza II.pdf
STIINTE ECONOMICE SI ADMINISTRAREA AFACERILOR
/Portals/0/Studenti/Cazari 2014-2015/Cazari SEAA Faza I 2014.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazare FSEAA faza II.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazare FSEAA faza III.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari FSEAA Final.pdf
DESIGN DE PRODUS SI MEDIU
/Portals/0/Studenti/Cazari 2014-2015/Cazare DPM faza I 2014.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazare DPM faza II.pdf
LITERE
/Portals/0/Studenti/Cazari 2014-2015/Cazare LT faza I 2014.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari LT Faza II.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari Faza III LT.pdf
STIINTA SI INGINERIA MATERIALELOR
/Portals/0/Studenti/Cazari 2014-2015/Cazari SIM Faza I 2014_2015.pdf
/Portals/0/Studenti/Cazari SIM faza II.pdf
DREPT
/Portals/0/Studenti/Cazari 2014-2015/Cazari drept 2014 Faza 1.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazare drept faza II.pdf
INGINERIE ELECTRICA SI STIINTA CALCULATOARELOR
/Portals/0/Studenti/Cazari 2014-2015/cazari_IESC_2014_2015_faza I.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari IESC faza II.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari IESC lista finala.pdf
SOCIOLOGIE SI COMUNICARE
/Portals/0/Studenti/Cazari 2014-2015/cazari socio 2014.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari SC faza II.pdf
SILVICULTURA SI EXPLOATARI FORESTIERE
/Portals/0/Studenti/Cazari 2014-2015/CAZARI SEF 2014-2015.pdf
MEDICINA
/Portals/0/Studenti/Cazari 2014-2015/Cazare faza 1 MD 2014.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazare MD faza II.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazare MD faza III.pdf
INGINERIA LEMNULUI
/Portals/0/Studenti/Cazari 2014-2015/Cazare IL faza I 2014.pdf
PSIHOLOGIE SI STIINTELE EDUCATIEI
/Portals/0/Studenti/Cazari 2014-2015/CAZARI Faza I FPSE.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari FPSE Faza II.pdf
CONSTRUCTII
/Portals/0/Studenti/Cazari 2014-2015/Cazari CT 2014 faza I.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari CT faza II.pdf
EDUCATIE FIZICA SI SPORTURI MONTANE
/Portals/0/Studenti/Cazari 2014-2015/Cazari EFSM faza I 2014.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari EFSM faza II.pdf
ALIMENTATIE SI TURISM
/Portals/0/Studenti/Cazari 2014-2015/Cazari faza I AT 2014.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari AT faza II.pdf
/Portals/0/Studenti/Cazari 2014-2015/Cazari AT faza III.pdf
MUZICA
/Portals/0/Studenti/Cazari 2014-2015/Cazare muzica faza I 2014.pdf
```

### Download accomodations for each faculty
`while read line; do echo "$line"; url="$(echo "$line" | cut -d'@' -f3)"; wget -N -P "data/accomodations/$(echo $line | cut -d'@' -f1)-$(echo $line | cut -d'@' -f2)" "$url"; done < <(xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/strong/span/text() | //*[@id="Facultati"]/tbody/tr/td/p/strong/a/@href' --data=data/cazarecamine.html | gawk '!a[$0]++' | gawk -F'\n' 'BEGIN { x=""; c=0; } { if (match($0, /^\//)) { print c "@" x "@" "http://www.unitbv.ro" $0;  } else { c+=1; x=$0; } }');`

### Import accomodation PDFs for each faculty into PostgreSQL
`xidel -q -e '//*[@id="Facultati"]/tbody/tr/td/p[1]/strong/span/text() | //*[@id="Facultati"]/tbody/tr/td/p/strong/a/@href' --data=data/cazarecamine.html | gawk '!a[$0]++' | gawk -F'\n' 'BEGIN { x=""; c=0; } { if (match($0, /^\//)) { phase=""; if (match($0, /((([fF]aza|FAZA)[ ][I1]+)|[fF]inal)/, phases)) { phase = phases[1]; } else { phase = "final" } sub(/1/, "I", phase); sub(/2/, "II", phase); sub(/3/, "III", phase); sub(/4/, "IV", phase); phase=toupper(phase); if (match($0, /\/Cazari ([0-9]+\-[0-9]+)\//, matches)) { year=matches[1]; print c "@" phase "@" "http://www.unitbv.ro" $0 "@" year; } else { print c "@" phase "@" "http://www.unitbv.ro" $0 "@" year; } } else { c+=1; x=$0; } }' | psql -h localhost -d studnet -U postgres -c "\COPY accomodations_raw(faculty_id, phase, url, period) FROM STDIN WITH CSV DELIMITER AS '@'"`

### Find how many accomodations PDFs there are
`echo "Number of accommodation pdfs: $(find data/accomodations -type f -name '*.pdf' -print | wc -l)"`

### Use [Tabula](https://github.com/tabulapdf/tabula) to extract accomodation data from each PDF into a CSV file 

### Import student accomodation data into PostgreSQL
`psql -h localhost -d studnet -U postgres -c "\COPY student_accomodations(faculty_id, student_name, campus_number, room_number, specialization, observation) FROM '/home/lynx/Projects/GitHub/Me/BrasovCampusStudentFinder/data/Camin10.csv' WITH CSV DELIMITER AS ','"`

psql -h localhost -d studnet -U postgres -c "\COPY student_accomodations(faculty_id, student_name, campus_number, room_number, specialization, observation) FROM '/home/lynx/Projects/GitHub/Me/BrasovCampusStudentFinder/data/Camin16.csv' WITH CSV DELIMITER AS ','"

### Import student enrollments data into PostgreSQL
psql -h localhost -d studnet -U postgres -c "\COPY student_enrollments(name, email, facultatea, ciclul_de_invatamant, forma_de_invatamant, domeniul, an_de_studiu, programul_de_studii, grupa) FROM '/home/lynx/Projects/GitHub/Me/BrasovCampusStudentFinder/lista_studenti_inrolati.csv' WITH HEADER CSV DELIMITER AS ','"

### Import faculties staff data into PostgreSQL
psql -h localhost -d studnet -U postgres -c "\COPY faculties_staff(email, identifier, lastname, firstname, department_id, job_id, leadership_id) FROM '/home/lynx/Projects/GitHub/Me/BrasovCampusStudentFinder/lista_email_profesori.csv' WITH HEADER CSV DELIMITER AS ','"

### Import student groups data into PostgreSQL
psql -h localhost -d studnet -U postgres -c "\COPY student_groups(an_universitar, facultate, ciclu_de_invatamant, forma_de_invatamant, domeniu, program_de_studii, an_de_studiu, grupa, email) FROM '/home/lynx/Projects/GitHub/Me/BrasovCampusStudentFinder/lista_email_grupe.csv' WITH HEADER CSV DELIMITER AS ','"

### Generate student users
insert into users(user_id, password_hash, role_id) select id, crypt('unitbv', gen_salt('bf', 8)), (select id from roles where name = 'student') from student_enrollments; 

### Number of rooms and students per floor
* Number of rooms (43):   
    * `for i in $(seq 301 343); do echo $i; done`
    * Left side: 323 [decrease to] 301  
    * Right side: 324 [increase to] 343  
* Number of students (86): `echo $((((323-301)+(343-324) + 2)*2))`

### Lista de email grupe studenti
http://portal.unitbv.ro/Default.aspx?tabid=294