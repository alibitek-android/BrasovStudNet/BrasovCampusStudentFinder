-- The latest accomodation data for each faculty
select distinct on (f.name) f.name as faculty_name, f.logo as faculty_logo, phase as accomodation_phase, url as accomodation_url, year as accomodation_period 
from accomodations a join faculties f on a.faculty_id = f.id order by f.name, accomodation_period desc, phase desc

-- Show students from all faculties from a campus
select f.name as faculty, s.name as student, s.room, s.specialization, s.obs 
from students s join faculties f on s.faculty_id = f.id
where campus = 10
order by room  

copy (select f.name as faculty, s.name as student, s.room, s.specialization, s.obs 
from students s join faculties f on s.faculty_id = f.id
where campus = 10
order by room) to '/tmp/campus10.csv' with delimiter ',' csv header;

-- Students for floor 0
select f.name as faculty, s.name as student, s.room, s.specialization, s.obs 
from students s join faculties f on s.faculty_id = f.id
where campus = 10 and length(to_char(room, 'FM999')) <= 2
order by room  

-- Missing rooms on floor 0
select gs.a as missing_room
from generate_series(1,99) gs(a)
left join (select distinct on (room) room
           from students s join faculties f on s.faculty_id = f.id 
           where campus = 10 and length(to_char(room, 'FM999')) <= 2
           order by room) f
on (f.room = gs.a)
where f.room is null


-- Students for floor 1
select f.name as faculty, s.name as student, s.room, s.specialization, s.obs 
from students s join faculties f on s.faculty_id = f.id
where campus = 10 and room::text ~ '^1' and length(to_char(room, 'FM999')) >= 3
order by room  

-- Missing rooms on floor 1
select gs.a as missing_room
from generate_series(101,143) gs(a)
left join (select distinct on (room) room
           from students s join faculties f on s.faculty_id = f.id 
           where campus = 10 and room::text ~ '^1' and length(to_char(room, 'FM999')) >= 3
           order by room) f
on (f.room = gs.a)
where f.room is null

/*
117
124
*/

-- Students for floor 2
select f.name as faculty, s.name as student, s.room, s.specialization, s.obs 
from students s join faculties f on s.faculty_id = f.id
where campus = 10 and room::text ~ '^2' and length(to_char(room, 'FM999')) >= 3
order by room  

-- Missing rooms on floor 2
select gs.a as missing_room
from generate_series(201,243) gs(a)
left join (select distinct on (room) room
           from students s join faculties f on s.faculty_id = f.id 
           where campus = 10 and room::text ~ '^2' and length(to_char(room, 'FM999')) >= 3
           order by room) f
on (f.room = gs.a)
where f.room is null

/*
214
217
224
*/

-- Students for floor 3
select f.name as faculty, s.name as student, s.room, s.specialization, s.obs 
from students s join faculties f on s.faculty_id = f.id
where campus = 10 and room::text ~ '^3' and length(to_char(room, 'FM999')) >= 3
order by room  

-- Missing rooms on floor 3
select gs.a as missing_room
from generate_series(301,343) gs(a)
left join (select distinct on (room) room
           from students s join faculties f on s.faculty_id = f.id 
           where campus = 10 and room::text ~ '^3' and length(to_char(room, 'FM999')) >= 3
           order by room) f
on (f.room = gs.a)
where f.room is null

/*
317
324
*/

-- Students for floor 4
select f.name as faculty, s.name as student, s.room, s.specialization, s.obs 
from students s join faculties f on s.faculty_id = f.id
where campus = 10 and room::text ~ '^4' and length(to_char(room, 'FM999')) >= 3
order by room  

-- Missing rooms on floor 4
select gs.a as missing_room
from generate_series(401,443) gs(a)
left join (select distinct on (room) room
           from students s join faculties f on s.faculty_id = f.id 
           where campus = 10 and room::text ~ '^4' and length(to_char(room, 'FM999')) >= 3
           order by room) f
on (f.room = gs.a)
where f.room is null

/*
402
403
405
406
409
411
413
415
417
418
424
425
*/

-- Find rooms with incorrect data
select s.room, count(*)
from students s join faculties f on s.faculty_id = f.id
where campus = 10 and room::text ~ '^3' and length(to_char(room, 'FM999')) >= 3
group by room
having count(*) > 2
order by room 

select f.name as faculty, s.name as student, s.room, s.specialization, s.obs  
from students s join faculties f on s.faculty_id = f.id 
where room in (337, 338, 343)
order by room

TRUNCATE students RESTART IDENTITY;
ALTER SEQUENCE students_id_seq RESTART WITH 1 MINVALUE 1;

-- Find rooms with only one student in them
select s.room, count(*)
from students s join faculties f on s.faculty_id = f.id
where campus = 10 and length(to_char(room, 'FM999')) >= 3
group by room
having count(*) = 1
order by room 

-- How many students for each faculty?
WITH counts AS (SELECT distinct on (f.name) f.name AS faculty, count(s.name) OVER (PARTITION BY f.name) FROM students s JOIN faculties f ON s.faculty_id = f.id)
SELECT *
FROM counts
ORDER BY count desc